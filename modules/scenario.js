export function textNodesFunction() {
  return textNodes;

}

const textNodes = [
  {
    id: 1,
    text: 'First scenario',
    options: [
      {
        text: 'Take goo',
        setState: { blueGoo: true},
        nextText: 2
      },
      {
        text: 'leave the goo',
      }
    ]
  },
  {
    id: 2,
    text: 'this is the second scenario.',
    options: [
      {
        text: 'trade the goo for a sword',
        requiredState: (currentState) => currentState.blueGoo,
        setState: { blueGoo: false, sword: true},
        nextText: 3
      },
      {
        text: 'trade the goo for a shield',
        requiredState: (currentState) => currentState.blueGoo,
        setState: { blueGoo: false, shield: true},
        nextText: 3
      },
      {
        text: 'ignore merchant',
        nextText: 4
      }
    ]
  },
  {
    id: 4,
    text: 'you died',
    options: [
      {
        text: 'restart the game',
        nextText: -1
      }
    ]

  }
];
