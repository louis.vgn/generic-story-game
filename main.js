import { textNodesFunction } from './modules/scenario.js'

const textElement = document.getElementById('text');
const buttonOptionsElement = document.getElementById('buttonOptions');

// keep track of what the character has on them

let state = {}
// function to start the game
function startGame() {
  state = {}
  showTextNode(1)

}

function showTextNode(textNodeIndex) {
  const textNode = textNodesFunction().find(textNode => textNode.id === textNodeIndex)
  textElement.innerText = textNode.text
  while (buttonOptions.firstChild) {
    buttonOptions.removeChild(buttonOptions.firstChild)
  }

  textNode.options.forEach(option => {
    if(showOption(option)) {
      const button = document.createElement('button')
      button.innerText = option.text
      button.classList.add('btn')
      button.addEventListener('click', () => selectOption(option))
      buttonOptions.appendChild(button)
    }
  })
}

function showOption(option) {
  return option.requiredState == null || option.requiredState(state)
}

// function to get the element clicked
function selectOption(option) {
  const nextTextNodeId = option.nextText
  if (nextTextNodeId <= 0) {
    return startGame()
  }
  state = Object.assign(state, option.setState)
  showTextNode(nextTextNodeId)
}

// call the function startGame to actually start the game
startGame();


// implement background images slideshow

// implement an automated download button to dowload a file
