# Generic Text Story Game

## Purpose 
You can create your own scenario/story that will work on your web browser as a small game.

## How to add your own story
In order to add your own story to the game, go to the `./modules/scenario.js` and edit it with the scenario you want and the options you want. 
